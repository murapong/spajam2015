﻿using Lean;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public ParticleSystem partilce;

    public Text gyroInfo;
    public Text dragTotalInfo;
    public Text scoreText;

    public Text ScoreView;

    Vector3 acceleration = Vector3.zero;

    // エリア
    public GameObject areaColliders;

    // 現在ターゲット
    public GameObject currentTargetBaby;

    // ベイビーズ(0,1,2,3)
    public GameObject[] babys;

    public GameObject babysMaster;

    bool isSakago = false;

    // ナデナデしたトータル
    float dragTotal = 0f;
    // 時間トータル
    float timeTotal = 0f;

    void Awake()
    {
        // 
        foreach (MeshRenderer mr in areaColliders.GetComponentsInChildren<MeshRenderer>())
        {
            mr.enabled = false;
        }

        partilce.enableEmission = false;
    }
    // Use this for initialization
    void Start()
    {
        ScoreManager.Instance.ResetScore();

        SoundManager.Instance.PlayGameBGM();

#if !UNITY_EDITOR
        gyroInfo.enabled = false;
        dragTotalInfo.enabled = false;
        scoreText.enabled = false;
#endif
    }

    int beforeLevel = 0;
    // Update is called once per frame
    void Update()
    {
        // ジャイロ情報
        acceleration = Input.acceleration;

        gyroInfo.text = acceleration.ToString();


        // 重力方向を変更
        Physics.gravity = acceleration * 10f;

        timeTotal += Time.deltaTime;

        // デバッグ表示
        dragTotalInfo.text = "Drag=" + dragTotal.ToString("0.00") + "\n" + "Time=" + timeTotal.ToString("0.00") + "\n" + isSakago;

        int currentLevel = 0;
        if ((dragTotal + timeTotal) > 25f) currentLevel = 1;
        if ((dragTotal + timeTotal) > 50f) currentLevel = 2;
        if ((dragTotal + timeTotal) > 75f) currentLevel = 3;

        if (beforeLevel != currentLevel)
        {
            partilce.Emit(100);
        }

        beforeLevel = currentLevel;


        // ナデナデと時間が100に達したら次のシーンへ
        if ((dragTotal + timeTotal) > 100f)
        {
            // ナデナデ回数をスコアに入れる
            int score = (int) dragTotal;
            // 逆子じゃない場合は点数が倍
            if (!isSakago) score *= 2;

            ScoreManager.Instance.AddScoreNum(score);
            // シーン遷移
            Application.LoadLevel(Scene.Game2);
        }

        ScoreView.text = ((int) dragTotal).ToString();
        if (!isSakago) ScoreView.text += "x2";

        // ベイビー切り替え
        foreach (GameObject go in babys)
        {
            go.SetActive(false);
        }

        // 現在ベイベー
        currentTargetBaby = babys[currentLevel];
        currentTargetBaby.SetActive(true);

        // 逆子判定
        if (currentTargetBaby != null)
        {
            // 上向き判定
            isSakago = (currentTargetBaby.transform.up.y > 0.0f);
        }

        // 脈動する
        myakudo = Mathf.Lerp(myakudo, 1.0f, Time.deltaTime * 5f);
        currentTargetBaby.transform.localScale = Vector3.one * myakudo;

        // 
        int tmp = (int) (dragTotal * 0.5f);
        if (tmp != beforeInt)
        {
            myakudo = 1.2f;
        }
        beforeInt = tmp;
    }
    int beforeInt = 0;
    float myakudo = 1.0f;

    void FixedUpdate()
    {
        // 中心にひかれる動き
        if (currentTargetBaby != null)
        {
            babysMaster.rigidbody.velocity += -babysMaster.transform.position.normalized * Time.deltaTime * 5f;
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;


        Gizmos.DrawLine(Vector3.zero, acceleration * 10f);
        //        Gizmos.DrawSphere(Vector3.zero, 1.0f);
    }






    /// <summary>
    /// スワイプイベント発生時
    /// </summary>
    /// <param name="finger"></param>
    public void OnFingerSwipe(LeanFinger finger)
    {
        //        ScoreManager.Instance.AddScore();

        //        int currentScore = ScoreManager.Instance.GetScore();
        //        scoreText.text = currentScore.ToString();

        //        if (currentScore >= 20)
        //            Application.LoadLevel(Scene.Result);
    }



    // ドラッグ情報
    public void OnFingerDrag(LeanFinger finger)
    {
        // 加算する
        dragTotal += finger.DeltaScreenPosition.magnitude / Screen.width;
    }

    protected virtual void OnEnable()
    {
        LeanTouch.OnFingerSwipe += OnFingerSwipe;
        LeanTouch.OnFingerDrag += OnFingerDrag;
    }

    protected virtual void OnDisable()
    {
        LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        LeanTouch.OnFingerDrag -= OnFingerDrag;
    }


}
