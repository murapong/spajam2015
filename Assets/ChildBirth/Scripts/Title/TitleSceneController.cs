﻿using UnityEngine;
using System.Collections;

public class TitleSceneController : MonoBehaviour
{
    /// <summary>
    /// スタートボタン押下時処理
    /// </summary>
    public void OnClick_Start()
    {
        SoundManager.Instance.PlaySelect();

        Application.LoadLevel(Scene.Game);
    }
    
    // Use this for initialization
    void Start()
    {
        SoundManager.Instance.PlayTileBGM();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
