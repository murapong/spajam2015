﻿
public class ScoreManager : Singleton<ScoreManager>
{
    /// <summary>
    /// スコア
    /// </summary>
    int score;

    /// <summary>
    /// スコア加算処理
    /// </summary>
    public void AddScore()
    {
        score++;
    }

    public void AddScoreNum(int point)
    {
        score += point;
    }



    /// <summary>
    /// スコア取得処理
    /// </summary>
    public int GetScore()
    {
        return score;
    }

    /// <summary>
    /// スコアリセット処理
    /// </summary>
    public void ResetScore()
    {
        score = 0;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
