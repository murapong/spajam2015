﻿using UnityEngine;
using System.Collections;

public class GameSceneController : MonoBehaviour
{
    /// <summary>
    /// リザルトボタン押下時処理（後で不要になるため削除）
    /// </summary>
    public void OnClick_Result()
    {
        Application.LoadLevel(Scene.Result);
    }

    // Use this for initialization
    void Start()
    {
	
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }
}
