﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Game2Manager : MonoBehaviour
{
    /// <summary>
    /// 残り時間
    /// </summary>
    public Text timeText;

    /// <summary>
    /// ボリュームの閾値
    /// </summary>
    const float volumeThreshold = 0.02f;

    /// <summary>
    /// クールタイムの閾値
    /// </summary>
    const float coolTimeThreshold = 0.5f;

    /// <summary>
    /// 制限時間
    /// </summary>
    const float limitTime = 20f;

    /// <summary>
    /// 入力回数
    /// </summary>
    int inputCount;

    /// <summary>
    /// 音声入力のクールタイム
    /// </summary>
    float coolTime;

    /// <summary>
    /// 経過時間
    /// </summary>
    float elapsedTime;

    TextSwitcher textSwitcher;

    VoiceReceiver voiceReceiver;

    // Use this for initialization
    void Start()
    {
        voiceReceiver = GameObject.FindGameObjectWithTag(Tag.VoiceReceiver).GetComponent<VoiceReceiver>();
        voiceReceiver.InitAudio();

        textSwitcher = GameObject.FindGameObjectWithTag(Tag.TextSwitcher).GetComponent<TextSwitcher>();
        textSwitcher.ShowHi1();

        timeText.text = "";
    }
	
    // Update is called once per frame
    void Update()
    {
        // 10秒前にカウントダウン
        if (limitTime - elapsedTime <= 10)
        {
            var sec = limitTime - (int) elapsedTime;
            timeText.text = "出産まで残り " + sec + " 秒";
        }

        elapsedTime += Time.deltaTime;
        if (elapsedTime >= limitTime)
            Application.LoadLevel(Scene.Result);
        
        coolTime += Time.deltaTime;
        if (coolTime < coolTimeThreshold)
            return;
       
        float vol = voiceReceiver.GetAveragedVolume();
        if (vol >= volumeThreshold)
            InputAudio();
        
        if (inputCount % 3 == 0)
        {
            // ひっ(小)
            textSwitcher.ShowHi1();
        }
        else if (inputCount % 3 == 1)
        {
            // ひっ(大)
            textSwitcher.ShowHi2();
        }
        else
        {
            // ふー
            textSwitcher.ShowFu();
        }
    }

    /// <summary>
    /// 音声入力処理
    /// </summary>
    void InputAudio()
    {
        inputCount++;

        coolTime = 0f;
    }
}
