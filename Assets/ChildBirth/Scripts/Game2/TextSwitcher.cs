﻿using UnityEngine;
using System.Collections;

public class TextSwitcher : MonoBehaviour
{
    /// <summary>
    /// ひっ(小)
    /// </summary>
    public GameObject Hi1;

    /// <summary>
    /// ひっ(大)
    /// </summary>
    public GameObject Hi2;

    /// <summary>
    /// ふー
    /// </summary>
    public GameObject Fu;

    // Use this for initialization
    void Start()
    {
	
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    /// <summary>
    /// ひっ(小)表示処理
    /// </summary>
    public void ShowHi1()
    {
        Hi1.SetActive(true);
        Hi2.SetActive(false);
        Fu.SetActive(false);
    }

    /// <summary>
    /// ひっ(大)表示処理
    /// </summary>
    public void ShowHi2()
    {
        Hi1.SetActive(false);
        Hi2.SetActive(true);
        Fu.SetActive(false);
    }

    /// <summary>
    /// ふー表示処理
    /// </summary>
    public void ShowFu()
    {
        Hi1.SetActive(false);
        Hi2.SetActive(false);
        Fu.SetActive(true);
    }
}
