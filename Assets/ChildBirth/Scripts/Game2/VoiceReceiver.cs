﻿using UnityEngine;
using System.Collections;

public class VoiceReceiver : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
	
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    /// <summary>
    /// オーディオの初期化処理
    /// </summary>
    public void InitAudio()
    {
        audio.clip = Microphone.Start(null, true, 999, 44100);
        audio.loop = true;
        audio.mute = true;
        while (!(Microphone.GetPosition("") > 0))
        {
        }
        audio.Play();          
    }

    /// <summary>
    /// 平均ボリューム取得処理
    /// </summary>
    /// <returns>The averaged volume.</returns>
    public float GetAveragedVolume()
    { 
        float[] data = new float[256];
        float a = 0;

        audio.GetOutputData(data, 0);
        foreach (float s in data)
            a += Mathf.Abs(s);

        return a / 256.0f;
    }
}
