﻿using UnityEngine;
using System.Collections;

public class ResultSceneController : MonoBehaviour
{
    /// <summary>
    /// リトライボタン押下時処理
    /// </summary>
    public void OnClick_Retry()
    {
        Application.LoadLevel(Scene.Game);
    }

    /// <summary>
    /// タイトルボタン押下時処理
    /// </summary>
    public void OnClick_Title()
    {
        Application.LoadLevel(Scene.Title);
    }

    // Use this for initialization
    void Start()
    {
        SoundManager.Instance.StopBGM();
	
        SoundManager.Instance.PlayFun();
    }
	
    // Update is called once per frame
    void Update()
    {
        
    }
}
