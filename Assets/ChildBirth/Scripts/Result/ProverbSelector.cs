﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ProverbSelector : MonoBehaviour
{
    public Text proverbText;
    
    List<string> proverbs = new List<string> {
        "どんだけ稼いでウマいもんが食えるようになってもカーチャン作った飯がやっぱり最高なんだよ",
        "「カーチャンには…長生きしてほしい？だったら'世話'焼かせてあげな…。その時間はきっとすげぇ大事なもんだ。",
        "忙しい忙しい言ってねぇで、実家にはちゃんと帰りな…そんで、元気な顔を見せたときのカーチャン、ちゃんと見ときな…",
        "元気な顔見せるだけで、カーチャンってのは安心するもんさ…無理なら無理でいい、電話くらいはしような…",
        "カーチャンの手、ちゃんと見てみな…それがお前さんを今まで育ててきた手だ…",
        "親孝行ってのは、親がいるうちにしかでねぇんだ…いなくなってからじゃ遅いんだ…今からでも遅くはねぇ",
        "…もうそろそろ、カーチャンの前で素直になってもいいんじゃねぇか？一言言うだけでいいんだ。何に対してとかじゃねぇ。「ありがとう」ってな",
        "カーチャンは世話焼きで心配性なもんさ…ただその時間も、いつまでも続かねぇんだ。",
        "カーチャンと買い物、か。昔ならウザったかったかもしれんが、今なら「行きたい」と思うんじゃねぇか？だったら素直になりな…。",
        "カーチャンの作った飯、おいしいと思ったなら素直に「おいしい」って言ってやりな…それだけでいい…",
    };

    // Use this for initialization
    void Start()
    {
        var index = Random.Range(0, proverbs.Count);
        proverbText.text = proverbs[index];
    }

    // Update is called once per frame
    void Update()
    {
	
    }
}
