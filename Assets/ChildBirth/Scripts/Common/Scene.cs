﻿using UnityEngine;
using System.Collections;

public class Scene
{
    /// <summary>
    /// Titleシーン
    /// </summary>
    public const string Title = "Title";

    /// <summary>
    /// Gameシーン
    /// </summary>
    public const string Game = "Game";

    /// <summary>
    /// Game2シーン
    /// </summary>
    public const string Game2 = "Game2";

    /// <summary>
    /// Resultシーン
    /// </summary>
    public const string Result = "Result";
}
