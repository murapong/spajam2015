﻿using UnityEngine;
using System.Collections;

public class Tag
{
    public const string ScoreText = "ScoreText";

    public const string TextSwitcher = "TextSwitcher";

    public const string VoiceReceiver = "VoiceReceiver";
}
