﻿using UnityEngine;
using System.Collections;

public class SoundManager : Singleton<SoundManager>
{

    #region AudioClip

    AudioClip bgmTitle;

    AudioClip bgmGame;

    AudioClip bgmResult;

    AudioClip select;

    AudioClip fun;

    #endregion

    #region AudioSource

    AudioSource audioSourceBGM;

    AudioSource audioSourceSelect;

    AudioSource audioSourceFun;

    #endregion

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        
        bgmTitle = Resources.Load("BGM/bgm_title") as AudioClip;
        bgmGame = Resources.Load("BGM/bgm_game") as AudioClip;
        bgmResult = Resources.Load("BGM/bgm_result") as AudioClip;
        audioSourceBGM = gameObject.AddComponent<AudioSource>();

        select = Resources.Load("SE/select") as AudioClip;
        audioSourceSelect = gameObject.AddComponent<AudioSource>();

        fun = Resources.Load("SE/muci_fan_03") as AudioClip;
        audioSourceFun = gameObject.AddComponent<AudioSource>();
    }

    void Start()
    {
        
    }

    void Update()
    {

    }

    /// <summary>
    /// タイトルBGMを再生する
    /// </summary>
    public void PlayTileBGM()
    {
        audioSourceBGM.clip = bgmTitle;
        audioSourceBGM.loop = true;
        audioSourceBGM.volume = 0.8f;
        audioSourceBGM.Play();
    }

    /// <summary>
    /// ゲームBGMを再生する
    /// </summary>
    public void PlayGameBGM()
    {
        audioSourceBGM.clip = bgmGame;
        audioSourceBGM.loop = true;
//        audioSourceBGM.volume = 0.8f;
        audioSourceBGM.Play();
    }

    /// <summary>
    /// リザルトBGMを再生する
    /// </summary>
    public void PlayResultBGM()
    {
        audioSourceBGM.clip = bgmResult;
        audioSourceBGM.loop = true;
        audioSourceBGM.Play();
    }

    /// <summary>
    /// BGMを停止する
    /// </summary>
    public void StopBGM()
    {
        audioSourceBGM.Stop();
    }

    /// <summary>
    /// 選択を再生する
    /// </summary>
    public void PlaySelect()
    {
        audioSourceSelect.clip = select;
        audioSourceSelect.Play();
    }

    /// <summary>
    /// ファンファーレを再生する
    /// </summary>
    public void PlayFun()
    {
        audioSourceFun.clip = fun;
        audioSourceFun.Play();
    }
}